import tweepy
import serial
import json
import sys 
consumer_key = 
consumer_secret = 
access_token = 
access_token_secret =

serA=serial.Serial('/dev/ttyACM0')
serA.baudrate=9600


#MOTS CLES par PROJET
#OK:
TRACK_TERMS_A=["ROSE", "VIOLET", "VERT", "ROUGE", "ORANGE", "JAUNE", "BLEU" , "BLEUE", "PINK", "PURPLE","GREEN","YELLOW","BLUE","RED"]
#OK:
TRACK_TERMS_B=["HITLER", "NAZI", "FUHRER", "NAZISME", "GESTAPO", "MEIN KAMPF" , "CAMP DE CONCENTRATION", "LA RAFLE", "WWII", "DEPORTATION", "AUSCHWITZ", "ARYEN" , "ADOLF" , "JEUNESSE HITLERIENNE" , "ANTISEMITE", "ANTISEMITISME"]
#OK:
TRACK_TERMS_C=["FRANCE" , "USA" , "JAPAN" , "INDIA" , "SPAIN" , "TURKEY" , "RUSSIA" , "CANADA", "SAUDI ARABIA", "SOUTH AFRICA", "SOUTH KOREA", "COLOMBIA", "UNITED KINGDOM", "BRASIL", "INDONESIA"]



#GROUPE D (8 categories)
#OK:
TRACK_TERMS_D_CUL=["CULTURE", "HISTOIRE", "ARCHITECTURE", "THEATRE", "ART", "PHOTO", "DESIGN", "CINEMA"]
TRACK_TERMS_D_ECO=["ECONOMIE", "EURO", "BOURSE", "INFLATION", "CAC40", "BANQUE", "ENTREPRISE", "CAPITAL"]
TRACK_TERMS_D_INS=["INSTITUTION", "GREENPEACE", "NATIONS UNIES", "ONU", "UNESCO", "ASSOCIATION", "EUROPE"]
TRACK_TERMS_D_LIF=["LIFESTYLE", "MODE", "STYLE", "GASTRONOMIE", "FOOD", "SANTE"]
TRACK_TERMS_D_MUS=["MUSIQUE", "CONCERT", "RADIO", "VEVO", "DEEZER", "SPOTIFY", "MUSIC"]
TRACK_TERMS_D_POL=["POLITIQUE", "ELECTION", "MINISTRE", "PRESIDENT", "HOLLANDE", "FILLON", "MELENCHON", "VALLS", "LEPEN"]
TRACK_TERMS_D_SPO=["SPORT", "BASKETBALL", "FOOTBALL", "NBA", "TENNIS", "SUPERBOWL", "LIGUE1", "LIGUE2"]
TRACK_TERMS_D_DIV=["PS4", "XBOX", "TPMP", "M6", "TF1", "CANAL", "QUOTIDIEN", "GOT", "TWD", "NETFLIX", "STREAMING"]
#-----------------------------------------
#GROUPE E (les mots cles n'ont pas le meme "poids/influence"

TRACK_TERMS_VIE1=["VIE","ENERGIE","BIEN"]
TRACK_TERMS_VIE2=["VIVANT","EXISTENCE","BEBE"]
TRACK_TERMS_VIE3=["NAISSANCE","FECONDATION"]
TRACK_TERMS_MOR1=["MORT","MORTEL","MAL"]
TRACK_TERMS_MOR2=["MOURIR","DECES","RIP"]
TRACK_TERMS_MOR3=["SUICIDE","AVORTEMENT"]
#VIE : VIE, VIVANT, NAISSANCE, EXISTENCE, BEBE, ENERGIE, FECONDATION, BIEN
#MORT : MORT, MOURIR, DECES, SUICIDE, AVORTEMENT, MORTEL, RIP, MAL
#motcles = MOR1,MOR2,MOR3... VIE1,VIE2




class StreamListener(tweepy.StreamListener):
	def on_status(self, status):
		if ('RT @'  in status.text):
			return
		description = status.user.description
		loc = status.user.location
		text = status.text
		coords = status.coordinates
		geo = status.geo
		name = status.user.screen_name
		user_created = status.user.created_at
		followers = status.user.followers_count
		id_str = status.id_str
		created = status.created_at
		retweets = status.retweet_count
		bg_color = status.user.profile_background_color
		if geo is not None:
			geo = json.dumps(geo)
		if coords is not None:
			coords = json.dumps(coords)
		try:
			for word in text.split(" "):
				#==========================================
				# PROJET A : COuleurs
				if word.upper() in TRACK_TERMS_A:
					print ("PROJET A : %s" % (word))
					if (word.upper() in ["PINK"]):
							#envoie MOTCLE sur le port serie de l'Arduino 1
							serA.write("PINK\n")
							print ("PROJET A : %s" % (word))
					if (word.upper()=="RED"):
							#envoie MOTRED sur le port serie de l'Arduino 1
							serA.write("ROUG\n")
							print ("PROJET A : %s" % (word))
					if (word.upper()=="BLUE"):
							#envoie MOTRED sur le port serie de l'Arduino 1
							serA.write("BLEU\n")
							print ("PROJET A : %s" % (word))
					if (word.upper()=="BLEU"):
							#envoie MOTRED sur le port serie de l'Arduino 1
							serA.write("BLEU\n")
							print ("PROJET A : %s" % (word))
					if (word.upper()=="BLEUE"):
							#envoie MOTRED sur le port serie de l'Arduino 1
							serA.write("BLEU\n")
							print ("PROJET A : %s" % (word))
					if (word.upper()=="JAUNE"):
							#envoie MOTRED sur le port serie de l'Arduino 1
							serA.write("JAUN\n")
							print ("PROJET A : %s" % (word))
					if (word.upper()=="YELLOW"):
							#envoie MOTRED sur le port serie de l'Arduino 1
							serA.write("JAUN\n")
							print ("PROJET A : %s" % (word))
					if (word.upper()=="VERT"):
							#envoie MOTRED sur le port serie de l'Arduino 1
							serA.write("VERT\n")
							print ("PROJET A : %s" % (word))
					if (word.upper()=="GREEN"):
							#envoie MOTRED sur le port serie de l'Arduino 1
							serA.write("VERT\n")
							print ("PROJET A : %s" % (word))
					if (word.upper()=="PURPLE"):
							#envoie MOTRED sur le port serie de l'Arduino 1
							serA.write("PURP\n")
							print ("PROJET A : %s" % (word))
					if (word.upper()=="VIOLET"):
							#envoie MOTRED sur le port serie de l'Arduino 1
							serA.write("PURP\n")
							print ("PROJET A : %s" % (word))
					if (word.upper()=="ORANGE"):
							#envoie MOTRED sur le port serie de l'Arduino 1
							serA.write("ORAN\n")
							print ("PROJET A : %s" % (word))
							
				#==========================================
				# PROJET B : Godwin
				if word.upper() in TRACK_TERMS_B:
					print ("PROJET B : %s" % (word))
					#envoyer "GOD" sur le bon port serie de l'Arduino 2
					serA.write("GOD\n")

				#==========================================
				# PROJET C : Nations
				if word.upper() in TRACK_TERMS_C:
					print ("PROJET C : %s" % (word))
					if (word.upper() == "FRANCE"):
							#envoie MOTCLE sur le port serie de l'Arduino 3
							serA.write("FRA\n")
							print ("PROJET A : %s" % (word))
					if (word.upper() == "INDIA"):
							#envoie MOTCLE sur le port serie de l'Arduino 3
							serA.write("IND\n")
							print ("PROJET A : %s" % (word))
					if (word.upper() == "UNITED KINGDOM"):
							#envoie MOTCLE sur le port serie de l'Arduino 3
							serA.write("UK\n")
							print ("PROJET A : %s" % (word))
					if (word.upper() == "USA"):
							#envoie MOTCLE sur le port serie de l'Arduino 3
							serA.write("USA\n")
							print ("PROJET A : %s" % (word))
					if (word.upper() == "SPAIN"):
							#envoie MOTCLE sur le port serie de l'Arduino 3
							serA.write("ESP\n")
							print ("PROJET A : %s" % (word))
					if (word.upper() == "JAPAN"):
							#envoie MOTCLE sur le port serie de l'Arduino 3
							serA.write("JAP\n")
							print ("PROJET A : %s" % (word))	
					if (word.upper() == "SOUTH AFRICA"):
							#envoie MOTCLE sur le port serie de l'Arduino 3
							serA.write("AFR\n")
							print ("PROJET A : %s" % (word))	
					if (word.upper() == "SOUTH KOREA"):
							#envoie MOTCLE sur le port serie de l'Arduino 3
							serA.write("COR\n")
							print ("PROJET A : %s" % (word))	
					if (word.upper() == "SAUDI ARABIA"):
							#envoie MOTCLE sur le port serie de l'Arduino 3
							serA.write("ARA\n")
							print ("PROJET A : %s" % (word))		
					if (word.upper() == "TURKEY"):
							#envoie MOTCLE sur le port serie de l'Arduino 3
							serA.write("TUR\n")
							print ("PROJET A : %s" % (word))											
					if (word.upper() == "CANADA"):
							#envoie MOTCLE sur le port serie de l'Arduino 3
							serA.write("CAN\n")
							print ("PROJET A : %s" % (word))
					if (word.upper() == "RUSSIA"):
							#envoie MOTCLE sur le port serie de l'Arduino 3
							serA.write("RUS\n")
							print ("PROJET A : %s" % (word))
					if (word.upper() == "INDONESIA"):
							#envoie MOTCLE sur le port serie de l'Arduino 3
							serA.write("NDO\n")
							print ("PROJET A : %s" % (word))
					if (word.upper() == "BRASIL"):
							#envoie MOTCLE sur le port serie de l'Arduino 3
							serA.write("BRE\n")
							print ("PROJET A : %s" % (word))
					if (word.upper() == "COLOMBIA"):
							#envoie MOTCLE sur le port serie de l'Arduino 3
							serA.write("COL\n")
							print ("PROJET A : %s" % (word))						
							
				#==========================================
				# PROJET D : 
				if word.upper() in TRACK_TERMS_D_CUL:
					#envoie MOTCLE sur le port serie de l'Arduino 4
					serA.write("CUL\n")
					print ("PROJET D : %s" % (word))
				if word.upper() in TRACK_TERMS_D_DIV:
					#envoie MOTCLE sur le port serie de l'Arduino 4
					serA.write("DIV\n")
					print ("PROJET D : %s" % (word))
				if word.upper() in TRACK_TERMS_D_ECO:
					#envoie MOTCLE sur le port serie de l'Arduino 4
					serA.write("ECO\n")
					print ("PROJET D : %s" % (word))
				if word.upper() in TRACK_TERMS_D_INS:
					#envoie MOTCLE sur le port serie de l'Arduino 4
					serA.write("INS\n")
					print ("PROJET D : %s" % (word))
				if word.upper() in TRACK_TERMS_D_LIF:
					#envoie MOTCLE sur le port serie de l'Arduino 4
					serA.write("LIF\n")
					print ("PROJET D : %s" % (word))
				if word.upper() in TRACK_TERMS_D_MUS:
					#envoie MOTCLE sur le port serie de l'Arduino 4
					serA.write("MUS\n")
					print ("PROJET D : %s" % (word))
				if word.upper() in TRACK_TERMS_D_POL:
					#envoie MOTCLE sur le port serie de l'Arduino 4
					serA.write("POL\n")
					print ("PROJET D : %s" % (word))						
				if word.upper() in TRACK_TERMS_D_SPO:
					#envoie MOTCLE sur le port serie de l'Arduino 4
					serA.write("SPO\n")
					print ("PROJET D : %s" % (word))
				
				#==========================================
				# PROJET E : 
				if word.upper() in TRACK_TERMS_VIE1:
					#envoie MOTCLE sur le port serie de l'Arduino 5
					serA.write("VIE1\n")
					print ("PROJET E : %s" % (word))
				if word.upper() in TRACK_TERMS_VIE2:
					#envoie MOTCLE sur le port serie de l'Arduino 5
					serA.write("VIE2\n")
					print ("PROJET E : %s" % (word))
				if word.upper() in TRACK_TERMS_VIE3:
					#envoie MOTCLE sur le port serie de l'Arduino 5
					serA.write("VIE3\n")
					print ("PROJET E : %s" % (word))
				if word.upper() in TRACK_TERMS_MOR1:
					#envoie MOTCLE sur le port serie de l'Arduino 5
					serA.write("MOR1\n")
					print ("PROJET E : %s" % (word))
				if word.upper() in TRACK_TERMS_MOR2:
					#envoie MOTCLE sur le port serie de l'Arduino 5
					serA.write("MOR2\n")
					print ("PROJET E : %s" % (word))
				if word.upper() in TRACK_TERMS_MOR3:
					#envoie MOTCLE sur le port serie de l'Arduino 5
					serA.write("MOR3\n")
					print ("PROJET E : %s" % (word))
									
		except :
			e = sys.exc_info()[1]
			print( "<p>Error: %s</p>" % e )

	def on_error(self, status_code):
		print(status_code)
		if status_code == 420:
			return False

 
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token,access_token_secret)
api = tweepy.API(auth)

stream_listener = StreamListener()
stream = tweepy.Stream(auth=api.auth, listener=stream_listener)
stream.filter(track=TRACK_TERMS_A,async=True)
#stream.filter(track=TRACK_TERMS_A+TRACK_TERMS_B+TRACK_TERMS_C+TRACK_TERMS_VIE1+TRACK_TERMS_D_CUL+TRACK_TERMS_D_ECO+TRACK_TERMS_D_INS+TRACK_TERMS_D_LIF+TRACK_TERMS_D_MUS+TRACK_TERMS_D_POL+TRACK_TERMS_D_SPO+TRACK_TERMS_D_DIV+TRACK_TERMS_VIE2+TRACK_TERMS_VIE3+TRACK_TERMS_MOR1+TRACK_TERMS_MOR2+TRACK_TERMS_MOR3)
#stream.filter()
